Unix Date / Time Format
=======================

Src: <https://www.lifewire.com/display-date-time-using-linux-command-line-4032698>

Display the date using any or all of the following elements:

%a: abbreviated day name (i.e. mon, tue, wed)
%A: full day name (i.e. Monday, Tuesday, Wednesday)
%b or %h: abbreviated month name (i.e. jan, feb, mar)
%B: full month name (January, February, March)
%c: locales date and time (full date and time)
%C: century - displays the first two numbers of the year (i.e 19 for 1999 and 20 for 2020)
%d: day of month (i.e. 01, 02, 03)
%D: same as M/D/Y (i.e. 04/20/16)
%e: day of month padded (i.e. ' 1', ' 2')
%F: full date, same as yyyy-mm-dd
%H: hour (00, 01, 02, 21, 22, 23)
%I: hour (1,2,3,10,11,12)
%j: day of year (i.e. 243)
%k: hour padded (i.e. '1' becomes ' 1')
%l: hour padded (12 hour clock)
%m: month number (1,2,3)
%M: minute (1,2,3,57,58,59)
%n: new line
%N: nanoseconds
%p: AM or PM
%P: like %p but lowercase (ironically)
%r: locales 12 hour clock time
%R: 24 hour version of hour and minute
%s: seconds since 1970-01-01 00:00:00
%S: second (01,02,03, 57, 58, 59)
%t: a tab
%T: time same as %H:%M:%S
%u: day of week (1 is Monday, 2 is Tuesday etc)
%U: week number of year (assuming Sunday as first day of the week)

%V: ISO week number with Monday as the first day of the week
%w: day of week (0 is Sunday)
%W: week number of the year with Monday as the first day of the week
%x: locales date representation (12/31/2015)
%X: locales time representation (14:44:44)
%y: last two digits of year
%Y: year
%z: numeric time zone (i.e. -0400)
%:z: numeric time zone as follows (i.e. -04:00)
%::z: numeric time zone as follows (i.e. -04:00:00)
%Z: alphabetic time zone abbreviation (GMT)
-: a single hyphen prevents zero padding
_: a single underscore pads with spaces
0: pads with zeroes
^: use uppercase if possible
#: use opposite case if possible
